const _appVer = "v1.5.8";
export { existsSync } from "jsr:@std/fs@1.0.8";
export * as fmt from "jsr:@std/fmt@1.0.3/colors";
export * as log from "jsr:@std/log@0.221.0";
export { Application } from "jsr:@oak/oak@17.1.4";
export { Hono } from "jsr:@hono/hono@4.6.16";
export { open as opn } from "https://denopkg.com/hashrock/deno-opn@v2.0.1/mod.ts";
