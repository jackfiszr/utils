import type { Application, Hono } from "./deps.ts";
import { existsSync, fmt, opn } from "./deps.ts";
import getLogger from "./logger.ts";
export { default as getLogger } from "./logger.ts";

const log = await getLogger();

export function getProperty<T, K extends keyof T>(obj: T, key: K) {
  return obj[key];
}

type FmtFn = (str: string) => string;

type dlogOpts = {
  color: keyof typeof fmt;
  title?: string;
  mainMsg?: string;
  subMsg?: string;
  subColor?: keyof typeof fmt;
  subPrefix?: string;
};

export function dlog(options: dlogOpts) {
  const defaults: dlogOpts = {
    color: "white",
    subColor: "dim",
    subPrefix: " > ",
  };
  options = { ...defaults, ...options };
  const title = `\n${options.title}: ${options.mainMsg}`;
  const bold = fmt.bold as FmtFn;
  const color = getProperty(fmt, options.color) as FmtFn;
  console.log(bold(color(title)));
  if (options.subMsg && options.subColor) {
    const subColor = getProperty(fmt, options.subColor) as FmtFn;
    console.log(subColor(options.subPrefix + options.subMsg));
  }
}

export function mlog(msgs: Array<{ text: string; color: keyof typeof fmt }>) {
  const bold = fmt.bold as FmtFn;
  msgs.forEach((msg) => {
    const color = getProperty(fmt, msg.color) as FmtFn;
    console.log(bold(color(msg.text)));
  });
}

export function timeDiff(startTime: number, endTime: number): string {
  return new Date(endTime - startTime - 3600000).toTimeString().substring(0, 8);
}

export function verCheck(stdVer: string): boolean {
  const denoVer = Deno.version.deno;
  const verStr = JSON.stringify(Deno.version).replace(/[{"}]/g, "").replace(
    /(:|,)/g,
    "$1 ",
  );
  if (stdVer && stdVer === denoVer) {
    dlog({
      color: "gray",
      title: "VER",
      mainMsg: `${verStr}, std: ${stdVer}`,
      subMsg: "ok, wersje środowiska uruchomieniowego i bibliotek są zgodne",
    });
    return true;
  } else {
    dlog({
      color: "yellow",
      title: "UWAGA",
      mainMsg:
        "Wersja bibliotek nie jest zgodna z wersją środowiska uruchomieniowego!",
      subMsg: `${verStr}, std: ${stdVer}`,
    });
    return false;
  }
}

export function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function txtToCleanArr(fileTextContent: string): string[] {
  const trimAll = (line: string) => line.trim();
  const noEmpty = (line: string) => line.length > 0;
  const toUpper = (line: string) => line.toUpperCase();
  const remDups = (line: string, index: number, arr: string[]) => {
    if (arr.indexOf(line) !== index) {
      dlog({
        color: "red",
        title: line,
        mainMsg: "Pozycja występuje na liście więcej niż raz",
        subMsg:
          "każde następne wystąpienie tego numeru będzie sygnalizowane takim komunikatem",
      });
      return false;
    }
    return true;
  };
  return fileTextContent.split("\n").map(trimAll).filter(noEmpty).map(toUpper)
    .filter(remDups);
}

export async function pdfToTxt(
  pdfFilePath: string,
  config = { silent: false },
): Promise<string> {
  const programName = "pdftotext";
  let title = "JUŻ ISTNIEJE";
  const txtFilePath = pdfFilePath.replace(".pdf", ".txt");
  if (!existsSync(txtFilePath)) {
    title = "UTWORZONO";
    try {
      const command = new Deno.Command(programName, {
        args: [pdfFilePath, txtFilePath],
      });
      const child = command.spawn();
      await child.status;
    } catch (error) {
      log.error(
        `BŁĄD: Nie można przekonwertować pliku ${pdfFilePath} do pliku tekstowego\n${error} — upewnij się, że program ${programName} jest zainstalowany`,
      );
      Deno.exit(1);
    }
  }
  if (!existsSync(txtFilePath)) {
    throw new Error("Plik tekstowy nie został utworzony");
  } else if (!config.silent) {
    log.info(`${title}: ${txtFilePath}`, ["white", "italic"]);
  }
  return txtFilePath;
}

export function range(start = 1, stop: number, step = 1) {
  return Array.from(
    { length: (stop - start) / step + 1 },
    (_, i) => start + (i * step),
  );
}

export async function startApp(app: Application, {
  name = "App",
  user = "unknown",
  host = "localhost",
  port = 8080,
  open = true,
}: {
  name?: string;
  user?: string;
  host?: string;
  port?: string | number;
  open?: boolean;
} = {}) {
  const { deno: denoVer } = Deno.version;
  app.addEventListener("listen", async (/* { hostname, port } */) => {
    if (open) {
      await opn(`http://${host}:${port}`);
    }
    dlog({
      color: "underline",
      title: "Aplikacja",
      mainMsg: `${name} [Deno ${denoVer}]`,
      subMsg: `uruchomiona pod adresem http://` +
        fmt.yellow(`${host}:${port}`) +
        `\n > przez użytkownika ${fmt.bold(user.toUpperCase())}`,
    });
  });

  await app.listen(`${host}:${port}`);
}

export async function runCmd(cmdStr: string) {
  dlog({
    color: "dim",
    title: "WYKONUJĘ",
    mainMsg: cmdStr,
  });
  const cmdArr = cmdStr.split(" ");
  const cmd = cmdArr[0];
  const args = cmdArr.slice(1);
  const command = new Deno.Command(cmd, { args });
  const child = command.spawn();
  await child.status;
  dlog({
    color: "dim",
    title: "WYKONANO",
    mainMsg: cmdStr,
  });
}

export async function startHonoApp(app: Hono, {
  name = "AppName",
  user = "unknown",
  host = "localhost",
  port = 8080,
  open = true,
}: {
  name?: string;
  user?: string;
  host?: string;
  port?: number;
  open?: boolean;
} = {}) {
  const { deno: denoVer } = Deno.version;
  log.info(`Aplikacja Hono 🔥: ${name} [Deno ${denoVer}]`, [
    "bold",
    "white",
    "underline",
  ]);
  log.info(
    `uruchomiona pod adresem http://` +
      fmt.yellow(`${host}:${port}`) +
      `\n > przez użytkownika ${fmt.bold(user.toUpperCase())}`,
    ["gray"],
  );
  Deno.serve({ hostname: host, port }, app.fetch);
  if (open) {
    await opn(`http://${host}:${port}`);
  }
}
