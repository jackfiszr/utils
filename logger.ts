import { fmt, log } from "./deps.ts";

type FmtFn = (str: string) => string;

export default async function (logFilePath?: string) {
  const logConfig: log.LogConfig = {
    handlers: {
      console: new log.ConsoleHandler("DEBUG", {
        formatter: (logRecord) => {
          let { msg } = logRecord;
          const fmtArr = logRecord.args[0] as [keyof typeof fmt];
          fmtArr?.forEach((color) => {
            const fmtFn = fmt[color] as FmtFn;
            msg = fmtFn(msg);
          });
          return msg;
        },
      }),
    },
    loggers: {
      default: {
        level: "DEBUG",
        handlers: ["console", "file"],
      },
    },
  };
  if (logFilePath && logConfig.handlers) {
    logConfig.handlers.file = new log.FileHandler("ERROR", {
      filename: logFilePath,
      formatter: (_) => "{datetime} {levelName} {msg}",
    });
  }
  await log.setup(logConfig);
  return log.getLogger();
}
